package com.devcamp.authorbookapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.authorbookapi.models.Author;

@Service
public class AuthorService {
    Author author1 = new Author("Tacgia1", "tacgia1@gmail.com", 'm');
    Author author2 = new Author("Tacgia2", "tacgia2@gmail.com", 'f');
    Author author3 = new Author("Tacgia3", "tacgia3@gmail.com", 'f');
    Author author4 = new Author("Tacgia4", "tacgia4@gmail.com", 'f');
    Author author5 = new Author("Tacgia5", "tacgia5@gmail.com", 'm');
    Author author6 = new Author("Tacgia6", "tacgia6@gmail.com", 'f');
    public ArrayList<Author> getAllAuthors(){
        ArrayList<Author> authorList = new ArrayList<>();
        authorList.add(author1);
        authorList.add(author2);
        authorList.add(author3);
        authorList.add(author4);
        authorList.add(author5);
        authorList.add(author6);
        return authorList;
    }
    
    public ArrayList<Author> getAuthorBook1(){
        ArrayList<Author> authorBook1 = new ArrayList<>();
        authorBook1.add(author1);
        authorBook1.add(author2);
        return authorBook1;
    }
    public ArrayList<Author> getAuthorBook2(){
        ArrayList<Author> authorBook2 = new ArrayList<>();
        authorBook2.add(author3);
        authorBook2.add(author4);
        return authorBook2;
    }
    public ArrayList<Author> getAuthorBook3(){
        ArrayList<Author> authorBook3 = new ArrayList<>();
        authorBook3.add(author5);
        authorBook3.add(author6);
        return authorBook3;
    }
}
