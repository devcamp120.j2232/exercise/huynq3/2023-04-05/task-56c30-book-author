package com.devcamp.authorbookapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.authorbookapi.models.Book;

@Service
public class BookService extends AuthorService{
    Book book1 = new Book("Kamasutra", getAuthorBook1(), 120000, 2);
    Book book2 = new Book("Gone with the wind", getAuthorBook2(), 80000, 4);
    Book book3 = new Book("50 Sac Thai", getAuthorBook3(), 90000, 3);
    public ArrayList<Book> getAllBooks(){
        ArrayList<Book> bookList = new ArrayList<>();
        bookList.add(book1);
        bookList.add(book2);
        bookList.add(book3);

        return bookList;
    }
}
