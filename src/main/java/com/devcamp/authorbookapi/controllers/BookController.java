package com.devcamp.authorbookapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.authorbookapi.models.Book;
import com.devcamp.authorbookapi.services.BookService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class BookController {
    @Autowired
    BookService bookService;
    @GetMapping("/books")
    public ArrayList<Book> getAllBooksApi(){
        return bookService.getAllBooks();
    }
    @GetMapping("/book-quantity")
    public ArrayList<Book> getBookInventory(@RequestParam int quantityNumber){
        ArrayList<Book> books = new ArrayList<>();
        for (int i=0; i< bookService.getAllBooks().size(); i++){
            if (bookService.getAllBooks().get(i).getQty() >= quantityNumber){
                books.add(bookService.getAllBooks().get(i));
            }
        }
        return books;
    }
}
